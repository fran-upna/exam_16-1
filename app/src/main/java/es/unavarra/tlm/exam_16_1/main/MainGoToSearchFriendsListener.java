package es.unavarra.tlm.exam_16_1.main;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import es.unavarra.tlm.exam_16_1.search.SearchActivity;

public class MainGoToSearchFriendsListener implements View.OnClickListener {

    private Context context;

    public MainGoToSearchFriendsListener(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this.context, SearchActivity.class);
        this.context.startActivity(intent);
    }
}
