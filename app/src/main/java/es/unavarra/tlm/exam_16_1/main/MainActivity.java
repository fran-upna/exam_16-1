package es.unavarra.tlm.exam_16_1.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import es.unavarra.tlm.exam_16_1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button searchButton = (Button) findViewById(R.id.search_button);
        searchButton.setOnClickListener(new MainGoToSearchFriendsListener(this));
    }
}
