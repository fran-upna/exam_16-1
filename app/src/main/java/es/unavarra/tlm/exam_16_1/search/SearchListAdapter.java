package es.unavarra.tlm.exam_16_1.search;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class SearchListAdapter extends BaseAdapter {

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Hay que usar R.layout.search_friend_row
        // Cuando el amigo nos sigue, usar como icono R.drawable.icon_is_friend y R.drawable.icon_empty cuando no
        return null;
    }
}
